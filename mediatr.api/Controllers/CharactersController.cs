﻿using Mediatr.Api.Models.Enums;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System.Threading.Tasks;
using static Mediatr.Api.Handlers.Characters;

namespace Mediatr.Api.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class CharactersController : Controller
  {
    private readonly ILogger log;
    private readonly IMediator _mediator;

    public CharactersController(ILogger logger, IMediator mediator)
    {
      log = logger.ForContext<CharactersController>();
      _mediator = mediator;
    }

    [HttpGet("characters/{type:int}")]
    public async Task<IActionResult> GetCharactersByClass([FromRoute] int type = default)
    {
      log.Information("Index called");

      var request = new RequestGetCharacters { ClassType = (ClassType)type};
      var result = await _mediator.Send(request);
      return Json(result);
    }
  }
}
