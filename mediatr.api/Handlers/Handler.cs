﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Mediatr.Api.Models;
using MediatR;

namespace Mediatr.Api.Handlers
{
  public partial class Characters
  {
    public class Handler : IRequestHandler<RequestGetCharacters, ResponseGetCharacters>
    {
      public async Task<ResponseGetCharacters> Handle(RequestGetCharacters request, CancellationToken cancellationToken)
      {
        await Task.CompletedTask;
        return new ResponseGetCharacters
        {
          Recipes = new List<Character>
          {
            new Character
            {
              Id = 100,
              Name = "Flintwinch"
            },
            new Character 
            {
              Id = 200,
              Name = "Foomanchu"
            }
          }
        };
      }
    }
  }
}
