﻿using Mediatr.Api.Models.Enums;
using MediatR;

namespace Mediatr.Api.Handlers
{
  public partial class Characters
  {
    public class RequestGetCharacters : IRequest<ResponseGetCharacters>
    {
      public ClassType ClassType { get; set; }
    }
  }
}
