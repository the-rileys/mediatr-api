﻿namespace Mediatr.Api.Models
{
  public class Ability
  {
    public int Id { get; set; }
    public string  Name { get; set; }
    public string Stats { get; set; }
  }
}
