﻿using System.Collections.Generic;
using Mediatr.Api.Models.Enums;

namespace Mediatr.Api.Models
{
  public class Character
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public ClassType Class { get; set; }
    public int Health { get; set; }
    public int HitPoints  { get; set; }
    public IEnumerable<Ability> Abilities { get; set; }
    public IEnumerable<Item> Items { get; set; }
  }
}
