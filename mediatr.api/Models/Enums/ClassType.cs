﻿namespace Mediatr.Api.Models.Enums
{
  public enum ClassType
  {
    Unknown = 0,
    Melee = 1,
    Ranged = 2,
    Caster = 3
  }
}
