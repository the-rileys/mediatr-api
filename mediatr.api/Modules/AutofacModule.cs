﻿using Autofac;
using Mediatr.Api.Repositories;
using Mediatr.Api.Services;

namespace Mediatr.Api.Modules
{
  public class AutofacModule : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterType<CharacterService>()
          .AsImplementedInterfaces()
          .SingleInstance();

      builder.RegisterType<CharacterRepository>()
          .AsImplementedInterfaces()
          .SingleInstance();

    }
  }
}
